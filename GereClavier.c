#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libISEN/GfxLib.h"
#include "libISEN/BmpLib.h"
#include "structure.h"
#include "GestionAffichage.h"
#include "GereClic.h"
#include "GereClavier.h"

/**
 * @brief      { Manage click on the keyboard }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Actual  pointer on Now struct
 * @param[in]  key     The key
 */
void manageNormalKeyboardMenu(gameParam *Game, Now *Actual, char key)
{
	confirm(Game, key);
	switch(Game->place) {
		case 0:
			manageNormalKeyboardRules(Game,key);
		break;

		case 1:
			manageQuitKey();
		break;

		case 2:
			manageQuitKey();
		break;

		case 3:
			manageWritingName(Game, Actual,key);
			manageQuitKey();
		break;

		case 4:
			manageQuitKey();
		break;
	}
}

/**
 * @brief      { Manage click when we write our name }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Actual  pointer on Now struct
 * @param[in]  key     The key
 */
void manageWritingName(gameParam *Game, Now *Actual, char key)
{
	switch(Game->type) {
		case 1:
			if(checkKey(key))
				write(Game, Actual);
			else if(key == 8)
				cut(Game, Actual);
			else if(key == 9)
				manageNames(Game,Actual);
		break;

		case 2:
			if(checkKey(key))
				write(Game, Actual);
			else if(key == 8)
				cut(Game, Actual);

		break;

		case 3:
		break;
	}
}

/**
 * @brief      { Manage click on the ESC key to quit the game }
 */
void manageQuitKey()
{
	if(caractereClavier() == 27) {
		releaseImages();
		termineBoucleEvenements();
	}
}

/**
 * @brief      { Manage click on the ENTER key to confirm }
 *
 * @param      Game  pointer on gameParam struct
 * @param[in]  key   The key
 */
void confirm(gameParam *Game, char key)
{
	if(key == 13) {
		switch(Game->place) {
			case 1:
				Game->place++;
			break;

			case 2:
				if(Game->type != 0)
					Game->place++;
			break;

			case 3:
				if(checkNames(Game->P1.name, Game->P2.name))
					Game->place++;
			break;

			case 4:
				if(Game->P1.pos != Game->P2.pos) {
					Game->place++;
					Game->gameStatus.action = 1;
				}
			break;
		}
	}
}

/**
 * @brief      { Manage click on the ESC to quit the rules }
 *
 * @param      Game  pointer on gameParam struct
 * @param[in]  key   The key
 */
void manageNormalKeyboardRules(gameParam *Game, char key) // A modifier quand on est en pleine partie Game->place peut changer.
{		
	if(key == 27) // Touche Echap
	{
		Game->page = 1;	
		Game->place = 1;
	}	
}

/**
 * @brief      { Compare 2 strings }
 *
 * @param      c1    The string 1
 * @param      c2    The string 2
 *
 * @return     { bool }
 */
bool checkNames(char *c1, char *c2)
{
	if(strlen(c1) > 0 && strlen(c2) > 0) {
		if(strcmp(c1,c2) == 0)
			return false;
		else
			return true;
	}
	else
		return false;
}

/**
 * @brief      { Verify if we pressed a good key to write the pseudo }
 *
 * @param[in]  key   The key
 *
 * @return     { bool }
 */
bool checkKey(char key)
{
	if(key == 45 || key == 32 || (key > 47 && key < 58) || (key > 64 && key < 91) || (key > 96 && key < 123))
		return true;
	else
		return false;
}

/**
 * @brief      { Write the pseudo }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Actual  pointer on Now struct
 */
void write(gameParam *Game, Now *Actual)
{
	char temp[1] = {caractereClavier()};

	if(Actual->chosing == 1) {
		if(strlen(Game->P1.name) < PSEUDO)
			strcat(Game->P1.name,temp);
	}
	else if(Actual->chosing == 2) {
		if(strlen(Game->P2.name) < PSEUDO)
		strcat(Game->P2.name,temp);
	}
}

/**
 * @brief      { Cut the last character of the pseudo }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Actual  pointer on Now struct
 */
void cut(gameParam *Game, Now *Actual)
{
	int i;
	char temp[PSEUDO];

	if(Actual->chosing == 1 && strlen(Game->P1.name) > 0) {
		for(i = strlen(Game->P1.name)-2; i >= 0; i--) {
			temp[i] = Game->P1.name[i];
		}
		temp[strlen(Game->P1.name)-1] = '\0';
		strcpy(Game->P1.name,temp);

	}

	else if(Actual->chosing == 2 && strlen(Game->P2.name) > 0) {
		for(i = strlen(Game->P2.name)-2; i >= 0; i--) {
			temp[i] = Game->P2.name[i];
		}
		temp[strlen(Game->P2.name)-1] = '\0';
		strcpy(Game->P2.name,temp);
	}
}

/**
 * @brief      { Switch player for writing name }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Actual  pointer on Now struct
 */
void manageNames(gameParam *Game, Now *Actual)
{
	if(Actual->chosing == 1)
		Actual->chosing++;
	else if(Actual->chosing == 2)
		Actual->chosing--;
}

/**
 * @brief      { Manage click on a special key (Directionnal keys for the rules) }
 *
 * @param      Game  pointer on gameParam struct
 * @param[in]  key   The key
 */
void manageSpecialKeyboardMenu(gameParam *Game, char key)	//int *place, int *selected, int *playerchosen "Lets set these data in a struct"
{
	printf("key : %d\n",key);
	switch(Game->place) {
		case 0:
			manageSpecialKeyboardRules(Game,key);
		break;

		case 1:
			manageQuitKey();
		break;

		case 2:
			manageSpecialKeyboardType(Game,key);
			manageQuitKey();
		break;

		case 3:
			manageQuitKey();
		break;

		case 4:
			manageSpecialKeyboardPlacement(Game,key);
			manageQuitKey();
		break;
	}
}

/**
 * @brief      { Manage clic on directional keys right and left to read every rules without using the mouse }
 *
 * @param      Game  pointer on gameParam struct
 * @param[in]  key   The key
 */
void manageSpecialKeyboardRules(gameParam *Game, char key)
{		
	if(key == 16 && Game->page < 4)
		Game->page++;
	else if(key == 15 && Game->page > 1)
		Game->page--;
	
}


/**
 * @brief      { Manage the selection of the type of game with directional keys }
 *
 * @param      Game  pointer on gameParam struct
 * @param[in]  key   The key
 */
void manageSpecialKeyboardType(gameParam *Game, char key)
{			
	int nb_button = 3;	// Nombre de boutons sur cet affichage

	if(Game->type == 0)
	{
		if(key == 16)
			Game->type = nb_button;
		else if(key == 15)
			Game->type = 1;
	}
	else
	{
		if(key == 16)
		{
			if(Game->type < nb_button)
				Game->type++;
		}
		else if(key == 15)
		{
			if(Game->type > 1)
				Game->type--;
		}
	}	
}

/**
 * @brief      { Manage the selection of the position of J1 et J2 with directional keys }
 *
 * @param      Game  pointer on gameParam struct
 * @param[in]  key   The key
 */
void manageSpecialKeyboardPlacement(gameParam *Game, char key)
{			
	if(key == 16){
		Game->P1.pos = 2;
		Game->P2.pos = 1;
	}
	else if(key == 15)
	{
		Game->P1.pos = 1;
		Game->P2.pos = 2;
	}	
}