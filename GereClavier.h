#ifndef KEYBOARD_H
#define KEYBOARD_H

void manageNormalKeyboardMenu(gameParam *Game, Now *Actual, char key);
	void manageQuitKey();
	void confirm(gameParam *Game, char key);
		bool checkNames(char *c1, char *c2);
	void manageNormalKeyboardRules(gameParam *Game, char key);
	void manageWritingName(gameParam *Game, Now *Actual, char key);
	bool checkKey(char key);
	void write(gameParam *Game, Now *Actual);
	void cut(gameParam *Game, Now *Actual);
	void manageNames(gameParam *Game, Now *Actual);

void manageSpecialKeyboardMenu(gameParam *Game, char key);
	void manageSpecialKeyboardRules(gameParam *Game, char key);
	void manageSpecialKeyboardType(gameParam *Game, char key);
	void manageSpecialKeyboardPlacement(gameParam *Game, char key);

#endif