#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libISEN/GfxLib.h"
#include "libISEN/BmpLib.h"
#include "structure.h"
#include "GestionAffichage.h"
#include "GereClic.h"
#include "GereClavier.h"
#include "board.h"

/**
 * @brief      { Initialize all parameters of the game and the board }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Actual  pointer on Now struct
 * @param      Board   pointer on boardParam struct
 */
void gameInitialisation(gameParam *Game, Now *Actual, boardParam *Board)
{
	Game->gameStatus.action = 0;
	Game->gameStatus.whoPlays = 1;
	
	Game->place = 1;
	Game->page = 1;
	Game->type = 0;

	Game->P1.win = 0;
	Game->P2.win = 0;
	Game->P1.pos = 0;
	Game->P2.pos = 0;
	strcpy(Game->P1.name,"");
	strcpy(Game->P2.name,"");

	Actual->chosing = 1;
	Actual->animation = 1;

	Board->pawnSelect.x = -1;
	Board->pawnSelect.y = -1;

	Checked.pos.x = Unchecked.pos.x = 427;
	Checked.pos.y = Unchecked.pos.y = 50;
	initBoard(Board);
}

/**
 * @brief      { Manage every clic }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Board   pointer on boardParam struct
 * @param      Actual  pointer on Now struct
 */
void manageClic(gameParam *Game, boardParam *Board, Now *Actual)
{
	switch(Game->gameStatus.action) {
		case 0:
			manageClicMenu(Game, Actual);
		break;

		case 1:// selection des pions
			if(checkClickOnBoard(Board))
			{
				pawnsPlacement(Game, Board);
				checkPlacement(Game, Board);
			}
			else{
				confirmPlacement(Game, Board);
				if(countPawnPlacement(Board, BOARD_DIM-1, 1) && countPawnPlacement(Board, BOARD_DIM-1, 2) && countPawnPlacement(Board, BOARD_DIM-1, 3))
					if(countPawnPlacement(Board, 0, 1) && countPawnPlacement(Board, 0, 2) && countPawnPlacement(Board, 0, 3))
						Game->gameStatus.action =2; // Tout les pions ont bien été posés
			}
				
		break;

		case 2:// Tout les pions sont placés, on commence à jouer.
			if(checkClickOnBoard(Board))
			{
				selectPawn(Game, Board);
				if(isThereAPawnSelected(Board)) {
					short tempState = Board->B[Board->pawnSelect.x][Board->pawnSelect.y].state;
					short tempPath = 0;
					resetPlaces(Board);
					setRoot(Board, Board->pawnSelect.x, Board->pawnSelect.y);
					calcMove(Game, Board, Board->pawnSelect.x, Board->pawnSelect.y, &tempState, &tempPath);
					movePawn(Game, Board);
					checkCage(Game, Board);
				}
			}
		break;

		case 3:// Fin de partie
			manageClicEndGame(Game, Board, Actual);
		break;
	}
}

/**
 * @brief      { Manage click on the menu and the rules }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Actual  pointer on Now struct
 */
void manageClicMenu(gameParam *Game,Now *Actual)
{
	switch(Game->place) {
		case 0:
			manageClicRules(Game);
			manageQuitGame();
		break;

		case 1:
			manageClicPlay(Game);
			manageQuitGame();
		break;

		case 2:
			manageClicKindParty(Game);
			manageQuitGame();
		break;

		case 3:
			manageClicName(Game,Actual);
			manageQuitGame();
		break;

		case 4:
			manageClicPosition(Game);
			manageQuitGame();
		break;
	}
}


/**
 * @brief      { Manage click to view every rules }
 *
 * @param      Game  The game
 */
void manageClicRules(gameParam *Game)
{		
	if(clicOnImg(RightEnable) && Game->page < 4)
		Game->page++;
	else if(clicOnImg(LeftEnable) && Game->page > 1)
		Game->page--;
	else if(clicOnImg(Back)) {
		Game->page = 1;
		Game->place = 1;
	}

}

/**
 * @brief      { Manage click on the off button }
 */
void manageQuitGame(void)
{
	if(clicOnImg(Off)) {
		releaseImages();
		termineBoucleEvenements();
	}
}

/**
 * @brief      { Manage click on the principal menu }
 *
 * @param      Game    pointer on gameParam struct
 */
void manageClicPlay(gameParam *Game)
{
	if(clicOnImg(Play))
		Game->place++;

	else if(clicOnImg(Rules))
		Game->place = 0;
}

/**
 * @brief      { Manage click to choose the type of game }
 *
 * @param      Game    pointer on gameParam struct
 */
void manageClicKindParty(gameParam *Game)
{
	if(clicOnImg(HumanvsHuman))
		Game->type = 1;
	else if(clicOnImg(HumanvsIA))
		Game->type = 2;
	else if(clicOnImg(IAvsIA))
		Game->type = 3;
	
	if(!clicOnImg(HumanvsHuman) && !clicOnImg(HumanvsIA) && !clicOnImg(IAvsIA) &&!clicOnImg(Checked))
		Game->type = 0;

	if(Game->type != 0)
	{
		if(clicOnImg(Checked))
			Game->place++;
	}
}

/**
 * @brief      { Manage click for the position of the player J1 }
 *
 * @param      Game    pointer on gameParam struct
 */
void manageClicPosition(gameParam *Game)
{
	if(clicOnImg(North)){
		Game->P1.pos = 1;
		Game->P2.pos = 2;
	}
	else if(clicOnImg(South)){
		Game->P1.pos = 2;
		Game->P2.pos = 1;		
	}


	if(!clicOnImg(North) && !clicOnImg(South) && !clicOnImg(Checked))
	{
		Game->P1.pos = 0;
		Game->P2.pos = 0;
	}
	
	if(Game->P1.pos != Game->P2.pos)
	{
		if(clicOnImg(Checked))
		{
			Game->place++;
			Game->gameStatus.action = 1;
		}
	}
}

/**
 * @brief      { Manage click on the name selection }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Actual   pointer on Now struct
 */
void manageClicName(gameParam *Game, Now *Actual)
{
	if(clicOnRect(40,200,400,260))
		Actual->chosing = 1;
	else if(clicOnRect(500,200,860,260))
		Actual->chosing = 2;

	switch(Game->type) {
		case 1:
			if(clicOnImg(Checked) && checkNames(Game->P1.name, Game->P2.name)) {
				demandeTemporisation(-1);
				Game->place++;
			}
		break;

		case 2:
		if(clicOnImg(Checked) && strlen(Game->P1.name) > 0) {
				demandeTemporisation(-1);
				Game->place++;
			}
		break;

		case 3:
		break;
	}
}

/**
 * @brief      { Check for each players if they had placed 2 pawns of each kind }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Board   pointer on boardParam struct
 */
void checkPlacement(gameParam *Game, boardParam *Board)
{
	if(Game->gameStatus.whoPlays == 1) {
		if(Game->P1.pos == 1) {
			if(countPawnPlacement(Board, BOARD_DIM-1, 1) && countPawnPlacement(Board, BOARD_DIM-1, 2) && countPawnPlacement(Board, BOARD_DIM-1, 3))
				Board->P1placement = 1;
			else
				Board->P1placement = 0;
		}
		else {
			if(countPawnPlacement(Board, 0, 1) && countPawnPlacement(Board, 0, 2) && countPawnPlacement(Board, 0, 3))
				Board->P1placement = 1;
			else
				Board->P1placement = 0;
		}
	}

	else {
		if(Game->P2.pos == 1) {
			if(countPawnPlacement(Board, BOARD_DIM-1, 1) && countPawnPlacement(Board, BOARD_DIM-1, 2) && countPawnPlacement(Board, BOARD_DIM-1, 3))
				Board->P2placement = 1;
			else
				Board->P2placement = 0;
		}
		else {
			if(countPawnPlacement(Board, 0, 1) && countPawnPlacement(Board, 0, 2) && countPawnPlacement(Board, 0, 3))
				Board->P2placement = 1;
			else
				Board->P2placement = 0;
		}
	}

}

/**
 * @brief      { Verify if there are 2 pawns of this state on the line i }   
 *
 * @param      Board   pointer on boardParam struct
 * @param[in]  i      { Line }
 * @param[in]  state  The value of the pawn
 *
 * @return     { bool }
 */
bool countPawnPlacement(boardParam *Board, int i, int state)
{
	short result = 0;
	for(int j = 0; j < BOARD_DIM; j++) {
		if(Board->B[i][j].state == state) {
			result++;
		}
	}
	if(result == 2)
		return true;
	else
		return false;
}

/**
 * @brief      { Verify click on the validate button }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Board   pointer on boardParam struct
 */
void confirmPlacement(gameParam *Game, boardParam *Board)
{
	if(Game->gameStatus.whoPlays == 1) {
		if(Board->P1placement == 1) {
			if(clicOnImg(Checked))
				switchPlayerTurn(Game);
		}
	}
	else {
		if(Board->P2placement == 1) {
			if(clicOnImg(Checked)) {
				switchPlayerTurn(Game);
			}
		}
	}
}

/**
 * @brief      { Switch the player turn }
 *
 * @param      Game    pointer on gameParam struct
 */
void switchPlayerTurn(gameParam *Game)
{
	Game->gameStatus.whoPlays = (Game->gameStatus.whoPlays % 2) + 1;
}

/**
 * @brief      { Verify the click at the pawn placement at the beginning of the game }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Board   pointer on boardParam struct
 */
void pawnsPlacement(gameParam *Game, boardParam *Board)
{
	if(Game->gameStatus.whoPlays == 1) {
		if(Game->P1.pos == 1)
			checkRect(Board, BOARD_DIM - 1);
		else
			checkRect(Board, 0);
	}
	else {
		if(Game->P2.pos == 1)
			checkRect(Board, BOARD_DIM - 1);
		else
			checkRect(Board, 0);
	}
}

/**
 * @brief      { Verify if we click on a box from the line i }
 *
 * @param      Board   pointer on boardParam struct
 * @param[in]  i      { Line of the board }
 */
void checkRect(boardParam *Board, short i) {
	int j;

	for(j = 0; j < BOARD_DIM; j++) {
		if(clicOnBoard(Board->B[i][j])) {
			if(Board->B[i][j].state < 3)
				Board->B[i][j].state++;
			else
				Board->B[i][j].state = 0;
		}
	}
}

/**
 * @brief      { Verify if we click on a box of the board }
 *
 * @param[in]  B     { The box in question }
 *
 * @return     { bool }
 */
bool clicOnBoard(box B) {
	if(abscisseSouris() > B.c.min.x && abscisseSouris() < B.c.max.x && ordonneeSouris() > B.c.min.y && ordonneeSouris() < B.c.max.y)
		return true;
	else
		return false;
}




/**
 * @brief      { Verify if we click on an image }
 *
 * @param[in]  img   The image
 *
 * @return     { bool }
 */
bool clicOnImg(Image img)
{
	if((abscisseSouris() > img.pos.x && abscisseSouris() < img.pos.x + (img.donnees)->largeurImage) && (ordonneeSouris() > img.pos.y && ordonneeSouris() < img.pos.y + (img.donnees)->hauteurImage))
		return true; // Clicked at the right place
	else
		return false; // Nope
}

/**
 * @brief      { Verify if we click on a rectangle }
 *
 * @param[in]  xmin  The xmin
 * @param[in]  ymin  The ymin
 * @param[in]  xmax  The xmax
 * @param[in]  ymax  The ymax
 *
 * @return     { bool }
 */
bool clicOnRect(float xmin, float ymin, float xmax, float ymax)
{
	if((abscisseSouris() > xmin && abscisseSouris() < xmax) && (ordonneeSouris() > ymin && ordonneeSouris() < ymax)) {
		printf("ok");
		return true; // Clicked at the right place
	}
	else
		return false; // Nope
}


/**
 * @brief      { Verify if we click on the board }
 *
 * @param      Board   pointer on boardParam struct
 *
 * @return     { bool }
 */
bool checkClickOnBoard(boardParam *Board)
{
 if(((abscisseSouris() > Board->B[0][0].c.min.x && abscisseSouris() < Board->B[BOARD_DIM-1][BOARD_DIM-1].c.max.x) && (ordonneeSouris() > Board->B[0][0].c.min.y && ordonneeSouris() < Board->B[BOARD_DIM-1][BOARD_DIM-1].c.max.y))
 	|| clicOnCage(Board, 1) || clicOnCage(Board, 0))
 {
 	return true;
 }
 else
 	return false;
}

/**
 * @brief      { Select a pawn on the nearest line from the player where there are pawns }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Board   pointer on boardParam struct
 */
void selectPawn(gameParam *Game, boardParam *Board)
{
	int ligne = -1;
	if(Game->gameStatus.whoPlays == 1){
		if(Game->P1.pos == 1)	// Nord
		{
			for(int i = BOARD_DIM - 1; i >= 0; i--)
			{
				for(int j = 0; j < BOARD_DIM; j++)
				{
					if(Board->B[i][j].state > 0 && ligne == -1) // le ligne ==-1 sinon on va prendre les lignes suivantes à cause du for...
					{
						ligne = i;
					}
				}
			}
			for(int j = 0 ;j < BOARD_DIM; j++){
				if(clicOnBoard(Board->B[ligne][j]) && Board->B[ligne][j].state > 0)
				{
					if (Board->pawnSelect.x == ligne && Board->pawnSelect.y == j) // On déselectionne
					{
						Board->pawnSelect.x = -1;
						Board->pawnSelect.y = -1;
					}
					else
					{
						Board->pawnSelect.x = ligne;
						Board->pawnSelect.y = j;
					}
				}
			}

		}
		else// Sud j1
		{
			for(int i = 0; i < BOARD_DIM; i++)
			{
				for(int j = 0; j < BOARD_DIM; j++)
				{
					if(Board->B[i][j].state > 0 && ligne == -1) // le ligne ==-1 sinon on va prendre les lignes suivantes à cause du for...
					{
						ligne = i;
					}
				}
			}
			for(int j = 0 ;j < BOARD_DIM; j++){
				if(clicOnBoard(Board->B[ligne][j])  && Board->B[ligne][j].state > 0)
				{
					if (Board->pawnSelect.x == ligne && Board->pawnSelect.y == j) // On déselectionne
					{
						Board->pawnSelect.x = -1;
						Board->pawnSelect.y = -1;
					}
					else
					{
						Board->pawnSelect.x = ligne;
						Board->pawnSelect.y = j;
					}
				}
			}
		}
	}
	else if(Game->gameStatus.whoPlays == 2)
	{
		if(Game->P2.pos == 1)	// Nord
		{
			for(int i = BOARD_DIM - 1; i >= 0; i--)
			{
				for(int j = 0; j < BOARD_DIM; j++)
				{
					if(Board->B[i][j].state > 0 && ligne == -1) // la condition ligne ==-1 sinon on va rentrer dans le if et prendre les lignes suivantes à cause du for...
					{
						ligne = i;
					}
				}
			}
			for(int j = 0 ;j < BOARD_DIM; j++){
				if(clicOnBoard(Board->B[ligne][j])  && Board->B[ligne][j].state > 0)
				{
					if (Board->pawnSelect.x == ligne && Board->pawnSelect.y == j) // On déselectionne
					{
						Board->pawnSelect.x = -1;
						Board->pawnSelect.y = -1;
					}
					else
					{
						Board->pawnSelect.x = ligne;
						Board->pawnSelect.y = j;
					}
				}
			}

		}
		else// Sud j2
		{
			for(int i = 0; i < BOARD_DIM; i++)
			{
				for(int j = 0; j < BOARD_DIM; j++)
				{
					if(Board->B[i][j].state > 0 && ligne == -1) // le ligne ==-1 sinon on va prendre les lignes suivantes à cause du for...
					{
						ligne = i;
					}
				}
			}
			for(int j = 0 ;j < BOARD_DIM; j++){
				if(clicOnBoard(Board->B[ligne][j])  && Board->B[ligne][j].state > 0)
				{
					if (Board->pawnSelect.x == ligne && Board->pawnSelect.y == j) // On déselectionne
					{
						Board->pawnSelect.x = -1;
						Board->pawnSelect.y = -1;
					}
					else
					{
						Board->pawnSelect.x = ligne;
						Board->pawnSelect.y = j;
					}
				}
			}
		}
	}
}



/**
 * @brief      Determines if there is a pawn selected.
 *
 * @param      Board   pointer on boardParam struct
 *
 * @return     True if there a pawn selected, False otherwise.
 */
bool isThereAPawnSelected(boardParam *Board)
{
	if(Board->pawnSelect.x == -1 &&	Board->pawnSelect.y == -1) {
		resetPlaces(Board);
		return false;
	}
	else
		return true;
}

/**
 * @brief      { Check if the cage is empty or not to end the game }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Board   pointer on boardParam struct
 */
void checkCage(gameParam *Game, boardParam *Board)
{
	if(Board->BCage[0].state != 0 || Board->BCage[1].state != 0){

		if(Board->BCage[0].state != 0){
			if(Game->P1.pos == 1)// Nord
			{
				Game->P1.win = 1;
			}
			else{
				Game->P2.win = 1;
			}
			Game->gameStatus.action = 3; // Il y a un gagnant --> La partie est finie

		}
		else if(Board->BCage[1].state != 0){
			if(Game->P1.pos == 1)// Nord
			{
				Game->P2.win = 1;
			}
			else{
				Game->P2.win = 1;
			}
			Game->gameStatus.action = 3; // Il y a un gagnant --> La partie est finie

		}
	}
	
}

/**
 * @brief      { Move a pawn }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Board   pointer on boardParam struct
 */
void movePawn(gameParam *Game, boardParam *Board)
{
	int i,j;

	if(Game->gameStatus.whoPlays == 1) {
		printf("salu 1t\n");
		if(Game->P1.pos == 1) {
			printf("salut 2\n");
			if(clicOnCage(Board, 0)) {
				printf("salut 3\n");
				if(Board->BCage[0].end == 1) {
					Board->BCage[0].state = Board->B[Board->pawnSelect.x][Board->pawnSelect.y].state;
					Board->B[Board->pawnSelect.x][Board->pawnSelect.y].state = 0;
				}
			}
		}
		else {
			if(clicOnCage(Board, 1)) {
				if(Board->BCage[1].end == 1) {
					Board->BCage[1].state = Board->B[Board->pawnSelect.x][Board->pawnSelect.y].state;
					Board->B[Board->pawnSelect.x][Board->pawnSelect.y].state = 0;
				}
			}
		}
	}
	else {
		if(Game->P2.pos == 1) {
			if(clicOnCage(Board,0)) {
				if(Board->BCage[0].end == 1) {
					Board->BCage[0].state = Board->B[Board->pawnSelect.x][Board->pawnSelect.y].state;
					Board->B[Board->pawnSelect.x][Board->pawnSelect.y].state = 0;
				}
			}
		}
		else {
			if(clicOnCage(Board, 1)) {
				if(Board->BCage[1].end == 1) {
					Board->BCage[1].state = Board->B[Board->pawnSelect.x][Board->pawnSelect.y].state;
					Board->B[Board->pawnSelect.x][Board->pawnSelect.y].state = 0;
				}
			}
		}
	}

	for(i = 0; i < BOARD_DIM; i++) {
		for(j = 0; j < BOARD_DIM; j++) {
			if(clicOnBoard(Board->B[i][j])) {
				printf("ok");
				if(Board->B[i][j].end == 1) {
					Board->B[i][j].state = Board->B[Board->pawnSelect.x][Board->pawnSelect.y].state;
					Board->B[Board->pawnSelect.x][Board->pawnSelect.y].state = 0;
					resetPlaces(Board);
					Board->pawnSelect.x = Board->pawnSelect.y = -1;
					switchPlayerTurn(Game);
				}
			}
		}
	}
}

/**
 * @brief      { Verify if the click is on a cage }
 * @param      Board   pointer on boardParam struct
 * @param[in]  i      { Number of the cage }
 *
 * @return     { bool }
 */
bool clicOnCage(boardParam *Board, int i)
{
	if(abscisseSouris() > Board->BCage[i].c.min.x && abscisseSouris() < Board->BCage[i].c.max.x && ordonneeSouris() > Board->BCage[i].c.min.y && ordonneeSouris() < Board->BCage[i].c.max.y)
		return true;
	else
		return false;

}

/**
 * @brief      { Manage click at the end of the game }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Board   pointer on boardParam struct
 * @param      Actual  pointer on Now struct
 */
void manageClicEndGame(gameParam *Game, boardParam *Board, Now *Actual)
{
	if(clicOnImg(Restart)){
		printf("Restart\n");
		restartGame(Game, Board, Actual);

	}
	else if(clicOnImg(EndOff))
	{
		printf("Shutting down !\n");
		releaseImages();
		termineBoucleEvenements();
	}
	else if(clicOnImg(PrincipalMenu))
	{
		printf("Menu Principal\n");
		gameInitialisation(Game, Actual, Board);
	}
}

/**
 * @brief      { Restart the game }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Board   pointer on boardParam struct
 * @param      Actual  pointer on Now struct
 */
void restartGame(gameParam *Game, boardParam *Board, Now *Actual)
{
	initBoard(Board);
	Game->gameStatus.action = 1;
	Game->gameStatus.whoPlays = 1;
	Game->P1.win = 0;
	Game->P2.win = 0;
	Actual->chosing = 1;
	Board->pawnSelect.x = -1;
	Board->pawnSelect.y = -1;
}