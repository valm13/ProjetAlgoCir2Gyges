#ifndef CLIC_H
#define CLIC_H

bool clicOnImg(Image img);
bool clicOnRect(float xmin, float ymin, float xmax, float ymax);
void pawnsPlacement(gameParam *Game, boardParam *Board);
void checkRect(boardParam *Board, short j);
bool clicOnBoard(box B);
void checkPlacement(gameParam *Game, boardParam *Board);
bool countStatesLine(box B);
void confirmPlacement(gameParam *Game, boardParam *Board);
void switchPlayerTurn(gameParam *Game);
bool countPawnPlacement(boardParam *Board, int i, int state);

void gameInitialisation(gameParam *Game, Now *Actual, boardParam *Board);

void manageClic(gameParam *Game, boardParam *Board, Now *Actual);
	void manageClicMenu(gameParam *Game,Now *Actual);
		void manageClicRules(gameParam *Game);
		void manageClicPlay(gameParam *Game);
		void manageClicKindParty(gameParam *Game);
		void manageClicPosition(gameParam *Game);
		void manageClicName(gameParam *Game, Now *Actual);
	void manageClicGame(gameParam *Game, boardParam *Board);

bool checkClickOnBoard(boardParam *Board);
void selectPawn(gameParam *Game, boardParam *Board);
bool isThereAPawnSelected(boardParam *Board);
void movePawn(gameParam *Game, boardParam *Board);
void checkCage(gameParam *Game, boardParam *Board);
bool clicOnCage(boardParam *Board, int i);

void manageClicEndGame(gameParam *Game, boardParam *Board, Now *Actual);
void restartGame(gameParam *Game, boardParam *Board, Now *Actual);
void manageQuitGame(void);

#endif