#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "libISEN/GfxLib.h"
#include "libISEN/BmpLib.h"
#include "libISEN/imageSF.h"
#include "structure.h"
#include "GestionAffichage.h"
#include "GereClic.h"
#include "GereClavier.h"

// Selectable
Image Rules;
Image Play;
Image Back;
Image Off;
Image North;
Image South;
Image Begin;
Image HumanvsHuman;
Image HumanvsIA;
Image IAvsIA;
// Info / Background
Image Bckgnd;
Image BckgndGame;
Image NameP1;
Image NameP2;
Image LocationP1;
Image Opponent;
Image RulesP1;
Image RulesP2;
Image RulesP3;
Image RulesP4;
// State
Image Checked;
Image Unchecked;
// Arrow
Image LeftEnable;
Image LeftDisable;
Image RightEnable;
Image RightDisable;
// Pawn
Image SimpleP;
Image DoubleP;
Image TripleP;
// Plateau
Image PlayBoard;

Image PrincipalMenu;
Image EndOff;
Image Restart;

/**
 * @brief      { Initialize the parameters of the structures Image }
 *
 * @param      Img   Pointer on Image struct
 * @param[in]  x     { coords }
 * @param[in]  y     { coords }
 * @param[in]  t     { type }
 * @param      C     { path of the images }
 */
void loadImage(Image *Img, short x, short y, short t, char C[50])
{
	Img->donnees = lisBMPRGB(C);
	Img->pos.x = x;
	Img->pos.y = y;
	Img->type = t;
}

/**
 * @brief      { Initialize all the needed images for the program }
 */
void imageInitialisation(void)
{
	// Play Board
	loadImage(&PlayBoard, 450, 0, 1, "Fond/Plateau.bmp");
	// All Pawns
	loadImage(&SimpleP, 0, 0, 1, "Pions/Resized/Simple.bmp");
	loadImage(&DoubleP, 0, 0, 1, "Pions/Resized/Double.bmp");
	loadImage(&TripleP, 0, 0, 1, "Pions/Resized/Triple.bmp");
	// Game background
	loadImage(&BckgndGame, 0, 0, 0, "Fond/Fond_Annexe.bmp");
	// Rules
	loadImage(&Rules, 650, 200, 1, "Fond/Boutons_Finaux/Resized/Cliquable/Regles.bmp");
	// Launch
	loadImage(&Play, 150, 200, 1, "Fond/Boutons_Finaux/Resized/Cliquable/Jouer.bmp");
	// Human VS Human
	loadImage(&HumanvsHuman, 183, 152, 1, "Fond/Boutons_Finaux/Resized/Cliquable/J_vs_j.bmp");
	// Human VS IA
	loadImage(&HumanvsIA, 383, 152, 1, "Fond/Boutons_Finaux/Resized/Cliquable/J_vs_ia.bmp");
	// IA VS IA
	loadImage(&IAvsIA, 583, 152, 1, "Fond/Boutons_Finaux/Resized/Cliquable/Ia_vs_ia.bmp");
	// Back to main menu
	loadImage(&Back, 70, 200, 1, "Fond/Boutons_Finaux/Resized/Cliquable/Return.bmp");
	// Quit the game
	loadImage(&Off, 830, 530, 1, "Fond/Boutons_Finaux/Resized/Cliquable/Off.bmp");
	// North button
	loadImage(&North, 230, 130, 1, "Fond/Boutons_Finaux/Resized/Cliquable/Nord.bmp");
	// South button
	loadImage(&South, 530, 130, 1, "Fond/Boutons_Finaux/Resized/Cliquable/Sud.bmp");
	// Begin to play
	loadImage(&Begin, 150, 200, 1, "Fond/Boutons_Finaux/Resized/Cliquable/Lancer.bmp");
	// Background
	loadImage(&Bckgnd, 0, 0, 0, "Fond/Fond_Principal.bmp");
	// Player 1's name
	loadImage(&NameP1, 60, 280, 1, "Fond/Boutons_Finaux/Resized/Infos/Pseudo_J1.bmp");
	//Player 2's name
	loadImage(&NameP2, 520, 280, 1, "Fond/Boutons_Finaux/Resized/Infos/Pseudo_J2.bmp");
	// Player 1's location
	loadImage(&LocationP1, 300, 350, 1, "Fond/Boutons_Finaux/Resized/Infos/Emplacement_J1.bmp");
	// Opponent type
	loadImage(&Opponent, 300, 350, 1, "Fond/Boutons_Finaux/Resized/Infos/Type_Adversaire.bmp");
	// Rules page 1
	loadImage(&RulesP1, 200, 10, 1, "Fond/Rules/Page1.bmp");
	// Rules page 2
	loadImage(&RulesP2, 200, 10, 1, "Fond/Rules/Page2.bmp");
	// Rules page 3
	loadImage(&RulesP3, 200, 10, 1, "Fond/Rules/Page3.bmp");
	// Rules page 4
	loadImage(&RulesP4, 200, 10, 1, "Fond/Rules/Page4.bmp");
	// Check button
	loadImage(&Checked, 427, 50, 1, "Fond/Boutons_Finaux/Resized/Validation/Checked.bmp");
	// Check button
	loadImage(&Unchecked, 427, 50, 1, "Fond/Boutons_Finaux/Resized/Validation/Checked_Disable.bmp");
	// Left arrows
	loadImage(&LeftEnable, 720, 200, 1, "Fond/Boutons_Finaux/Resized/Left_Right/Left.bmp");
	loadImage(&LeftDisable, 720, 200, 1, "Fond/Boutons_Finaux/Resized/Left_Right/Left_Disable.bmp");
	// Right arrows
	loadImage(&RightEnable, 810, 200, 1, "Fond/Boutons_Finaux/Resized/Left_Right/Right.bmp");
	loadImage(&RightDisable, 810, 200, 1, "Fond/Boutons_Finaux/Resized/Left_Right/Right_Disable.bmp");
	// Buttons End Game
	int offset1x = 55;
	int offset2x = 10;
	loadImage(&PrincipalMenu, 50+offset1x, 150, 1, "Fond/Boutons_Finaux/Resized/Cliquable/MenuPrincipal.bmp");
	loadImage(&Restart, 160+offset1x+offset2x, 90, 1, "Fond/Boutons_Finaux/Resized/Cliquable/Restart.bmp");
	EndOff.donnees = Off.donnees;
	EndOff.type = 1;
	EndOff.pos.x = 80+offset1x+offset2x;
	EndOff.pos.y = 90;
}

/**
 * @brief      { Release all the pre-loaded images of the program }
 */
void releaseImages(void)
{
	libereDonneesImageRGB(&(Rules.donnees));
	libereDonneesImageRGB(&(Play.donnees));
	libereDonneesImageRGB(&(Back.donnees));
	libereDonneesImageRGB(&(Off.donnees));
	libereDonneesImageRGB(&(North.donnees));
	libereDonneesImageRGB(&(South.donnees));
	libereDonneesImageRGB(&(Begin.donnees));
	libereDonneesImageRGB(&(Bckgnd.donnees));
	libereDonneesImageRGB(&(NameP1.donnees));
	libereDonneesImageRGB(&(NameP2.donnees));
	libereDonneesImageRGB(&(LocationP1.donnees));
	libereDonneesImageRGB(&(Opponent.donnees));
	libereDonneesImageRGB(&(RulesP1.donnees));
	libereDonneesImageRGB(&(RulesP2.donnees));
	libereDonneesImageRGB(&(RulesP3.donnees));
	libereDonneesImageRGB(&(RulesP4.donnees));
	libereDonneesImageRGB(&(Checked.donnees));
	libereDonneesImageRGB(&(Unchecked.donnees));
	libereDonneesImageRGB(&(LeftEnable.donnees));
	libereDonneesImageRGB(&(LeftDisable.donnees));
	libereDonneesImageRGB(&(RightEnable.donnees));
	libereDonneesImageRGB(&(RightDisable.donnees));
	libereDonneesImageRGB(&(HumanvsHuman.donnees));
	libereDonneesImageRGB(&(HumanvsIA.donnees));
	libereDonneesImageRGB(&(IAvsIA.donnees));
	libereDonneesImageRGB(&(BckgndGame.donnees));
	libereDonneesImageRGB(&(SimpleP.donnees));
	libereDonneesImageRGB(&(DoubleP.donnees));
	libereDonneesImageRGB(&(TripleP.donnees));
	libereDonneesImageRGB(&(PrincipalMenu.donnees));
	libereDonneesImageRGB(&(Restart.donnees));
}

/**
 * @brief      { Display pawns of the board }
 *
 * @param      Board  pointer on boardParam struct
 */
void displayPawns(boardParam *Board) {
	int i,j;
	couleur bg;

	bg.r = 252;
	bg.v = 0;
	bg.b = 144;

	for(i = 0; i < BOARD_DIM; i++) {
		for(j = 0; j < BOARD_DIM; j++) {
			switch(Board->B[i][j].state) {
				case 0:

				break;

				case 1:
					ecrisImageSansFond(Board->B[i][j].c.min.x-2, Board->B[i][j].c.min.y-2, (SimpleP.donnees)->largeurImage, (SimpleP.donnees)->hauteurImage, (SimpleP.donnees)->donneesRGB, bg);
				break;

				case 2:
					ecrisImageSansFond(Board->B[i][j].c.min.x-2, Board->B[i][j].c.min.y-2, (DoubleP.donnees)->largeurImage, (DoubleP.donnees)->hauteurImage, (DoubleP.donnees)->donneesRGB, bg);
				break;

				case 3:
					ecrisImageSansFond(Board->B[i][j].c.min.x-2, Board->B[i][j].c.min.y-2, (TripleP.donnees)->largeurImage, (TripleP.donnees)->hauteurImage, (TripleP.donnees)->donneesRGB, bg);
				break;
			}
		}
	}
	for(i = 0; i < 2; i++) { // On affiche les points qui sont dans les cages
		switch(Board->BCage[i].state) {
				case 0:

				break;

				case 1:
					ecrisImageSansFond(Board->BCage[i].c.min.x, Board->BCage[i].c.min.y, (SimpleP.donnees)->largeurImage, (SimpleP.donnees)->hauteurImage, (SimpleP.donnees)->donneesRGB, bg);
				break;

				case 2:
					ecrisImageSansFond(Board->BCage[i].c.min.x, Board->BCage[i].c.min.y, (DoubleP.donnees)->largeurImage, (DoubleP.donnees)->hauteurImage, (DoubleP.donnees)->donneesRGB, bg);
				break;

				case 3:
					ecrisImageSansFond(Board->BCage[i].c.min.x, Board->BCage[i].c.min.y, (TripleP.donnees)->largeurImage, (TripleP.donnees)->hauteurImage, (TripleP.donnees)->donneesRGB, bg);
				break;
			}
	}
}

/**
 * @brief      { Display the default aspect of the beginning of the game }
 *
 * @param      Game   pointer on gameParam struct
 * @param      Board  pointer on boardParam struct
 */
void displayGame(gameParam *Game, boardParam *Board)
{
	dot coordN;
	dot coordS;

	Checked.pos.x = Unchecked.pos.x = 300;
	Checked.pos.y = Unchecked.pos.y = 100;
	coordN.x = 720;
	coordN.y = 555;
	if(Game->P1.pos == 1)
		coordS.x = 610-tailleChaine(Game->P2.name,22);
	else
		coordS.x = 610-tailleChaine(Game->P1.name,22);
	coordS.y = 30;

	displayPicture(Off);
	displayPicture(BckgndGame);
	displayPicture(PlayBoard);

	couleurCourante(50, 50, 50);
	epaisseurDeTrait(2);
	if(Game->P1.pos == 1) {
		afficheChaine(Game->P1.name, 22, coordN.x, coordN.y);
		afficheChaine(Game->P2.name, 22, coordS.x, coordS.y);
	}
	else {
		afficheChaine(Game->P1.name, 22, coordS.x, coordS.y);
		afficheChaine(Game->P2.name, 22, coordN.x, coordN.y);
	}

	if(Game->gameStatus.action == 1) {
		if(Game->gameStatus.whoPlays == 1) {
			if(Board->P1placement == 1) {
				displayPicture(Checked);
			}
			else
				displayPicture(Unchecked);
		}
		else {
			if(Board->P2placement == 1) {
				displayPicture(Checked);
			}
			else
				displayPicture(Unchecked);
		}
	}
}

/**
 * @brief      { Display case where we can play this pawn }
 *
 * @param      Board  pointer on boardParam struct
 */
void displayPlayablePlaces(boardParam *Board) {
	int i,j;
	color c;
	c.r = 255;
	c.g = c.b = 0;

	for(i = 0; i < BOARD_DIM; i++) {
		for(j = 0; j < BOARD_DIM; j++) {
			if(Board->B[i][j].end == 1)
				framePawns(Board, i, j);
		}
	}
	for(i = 0; i < 2; i++) {
		if(Board->BCage[i].end == 1)
			frameButton(Board->BCage[i].c.min.x, Board->BCage[i].c.min.y, Board->BCage[i].c.max.x, Board->BCage[i].c.max.y, 2, c);
	}
}

/**
 * @brief      { Manage display of the game }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Board   pointer on boardParam struct
 * @param      Actual  pointer on Now struct
 */
void manageDisplay(gameParam *Game, boardParam *Board, Now *Actual) {
	switch(Game->gameStatus.action) {
		case 0:
			displayMenu(Game, Actual);
		break;

		case 1:
			displayGame(Game, Board);
			displayPawns(Board);
			if(Board->P1placement == 1 && Game->gameStatus.whoPlays == 2)
				hidePawns(Game);
			
		break;

		case 2:
			displayGame(Game, Board);
			displayPawns(Board);
			displaySelectedPawns(Board);
			displayPlayablePlaces(Board);
		break;

		case 3:
			displayGame(Game, Board);
			displayPawns(Board);
			displayEnd(Game);
		break;
	}
}

/**
 * @brief      { Main function for the menu management }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Actual  pointer on Now struct
 */
void displayMenu(gameParam *Game, Now *Actual)
{
	manageParam(Game, Actual);
	switch(Game->place) {
		case 0:
			displayRules(Game);
		break;

		case 1:
			displayMainMenu();//WORK
		break;

		case 2:
			displayGameType(Game);//WORK
		break;

		case 3:
			manageDisplayName(Game, Actual);
		break;

		case 4:
			displayPlacementChoice(Game);
		break;
	}
}

/**
 * @brief      { Function that checkes the "gameParam" struct }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Actual  pointer on Now struct
 */
void manageParam(gameParam *Game, Now *Actual)
{
	if(Game->place < 0)
		Game->place = 0;
	else if(Game->place > 6)
		Game->place = 6;	
}

/**
 * @brief      { Display the different images of the game's rules }
 *
 * @param      Game  pointer on gameParam struct
 */
void displayRules(gameParam *Game)
{
	displayBackMenu();
	displayPicture(Back);
	switch(Game->page) {
		case 1:
			displayPicture(RulesP1);
		break;

		case 2:
			displayPicture(RulesP2);
		break;

		case 3:
			displayPicture(RulesP3);
		break;

		case 4:
			displayPicture(RulesP4);
		break;
	}
	directionButton(Game);

}

/**
 * @brief      { Dispalys the LEFT/RIGHT buttons enabled or not }
 *
 * @param      Game  pointer on gameParam struct
 */
void directionButton(gameParam *Game)
{
	int min = 1;
	int max = 4;
	if(Game->page <= min)
		displayPicture(LeftDisable);
	else
		displayPicture(LeftEnable);

	if(Game->page >= max)
		displayPicture(RightDisable);
	else
		displayPicture(RightEnable);
}

/**
 * @brief      { Displays the main (and first) menu of the program }
 */
void displayMainMenu()
{
	displayBackMenu();

	displayPicture(Rules);
	displayPicture(Play);
}

/**
 * @brief      { Displays the "type of game" options }
 *
 * @param      Game  pointer on gameParam struct
 */
void displayGameType(gameParam *Game)
{

	displayBackMenu();

	displayPicture(Opponent);

	displayPicture(HumanvsHuman);
	displayPicture(HumanvsIA);
	displayPicture(IAvsIA);
	selectionType(Game);
	
}

/**
 * @brief      { Displays the CHECKED/UNCHECKED buttons depending on the player's choices }
 *
 * @param      Game  pointer on gameParam struct
 */
void selectionType(gameParam *Game)
{
	if(Game->type != 0) {
		displayPicture(Checked);
		if(Game->type == 1)
			framePicture(HumanvsHuman);
		else if(Game->type == 2)
			framePicture(HumanvsIA);
		else if(Game->type == 3)
			framePicture(IAvsIA);
	}
	else {
		displayPicture(Unchecked);
	}
}

/**
 * @brief      { Displays the "placement" options }
 *
 * @param      Game  pointer on gameParam struct
 */
void displayPlacementChoice(gameParam *Game)
{
	displayBackMenu();

	displayPicture(LocationP1);
	displayPicture(North);
	displayPicture(South);

	selectionPlacementChoice(Game);
}

/**
 * @brief      { Displays the CHECKED/UNCHECKED buttons depending on the player's choices }
 *
 * @param      Game  pointer on gameParam struct
 */
void selectionPlacementChoice(gameParam *Game)
{
	if(Game->P1.pos != Game->P2.pos) {
		displayPicture(Checked);
		if(Game->P1.pos == 1)
			framePicture(North);
		else if(Game->P1.pos == 2)
			framePicture(South);
	}
	else {
		displayPicture(Unchecked);
	}
}

/**
 * @brief      { Manage the display of the "naming" options }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Actual  pointer on Now struct
 */
void manageDisplayName(gameParam *Game,Now *Actual)
{
	switch(Game->type) {
		case 1:
			displayNameMulti(Game, Actual);
		break;

		case 2:
			displayNameSolo(Game, Actual);
		break;

		case 3:
			// displayNameIA(Game);
		break;
	}
}

/**
 * @brief      { Displays the "naming" options for 2 players }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Actual  pointer on Now struct
 */
void displayNameMulti(gameParam *Game, Now *Actual)
{
	// For the bar animation
	demandeTemporisation(400);
				
	// Colors of the highliting
	color c;
	c.r = 100;
	c.b = 30;
	c.g = 75;
				
	displayBackMenu();
	displayPicture(NameP1);
	displayPicture(NameP2);
				
	// Gaps for the names
	couleurCourante(255, 255, 255);
	rectangle(40, 200, 400, 260);
	rectangle(500, 200, 860, 260);
	// Highliting
	frameButton(40 ,200, 400, 260, 4, c);
	frameButton(500, 200, 860, 260, 4, c);
				
	// Display of the names
	couleurCourante(55, 55, 55);
	epaisseurDeTrait(2);
	afficheChaine(Game->P1.name, 35, 45, 215);
	afficheChaine(Game->P2.name, 35, 505, 215);
				
	// Management of the bar (animation)
	if(Actual->chosing == 1) {	
		if(Actual->animation == 1)
			couleurCourante(0, 0, 0);
		else if(Actual->animation == 0)
			couleurCourante(255, 255, 255);
					
		epaisseurDeTrait(2);
		ligne(48+tailleChaine(Game->P1.name,35), 205, 48+tailleChaine(Game->P1.name,35), 255);
	}
	else if(Actual->chosing == 2) {		
		if(Actual->animation == 1)
			couleurCourante(0, 0, 0);
		else if(Actual->animation == 0)
			couleurCourante(255, 255, 255);
						
		epaisseurDeTrait(2);
		ligne(508+tailleChaine(Game->P2.name,35) ,205, 508+tailleChaine(Game->P2.name,35), 255);
	}
				
	// Shows if the names are "ok"
	if(checkNames(Game->P1.name, Game->P2.name))
		displayPicture(Checked);
	else
		displayPicture(Unchecked);
}

/**
 * @brief      { Displays the "naming" options for 1 player }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Actual  pointer on Now struct
 */
void displayNameSolo(gameParam *Game, Now *Actual)
{
	// For the bar animation
	demandeTemporisation(400);
				
	// Colors of the highliting
	color c;
	c.r = 100;
	c.b = 30;
	c.g = 75;
				
	displayBackMenu();
	NameP1.pos.x = 290;
	displayPicture(NameP1);
				
	// Gaps for the names
	couleurCourante(255,255,255);
	rectangle(270,200,630,260);
	// Highliting
	frameButton(270,200,630,260,4,c);
				
	// Display of the names
	couleurCourante(55,55,55);
	epaisseurDeTrait(2);
	afficheChaine(Game->P1.name,35,275,215);
				
	// Management of the bar (animation)
	if(Actual->chosing == 1) {	
		if(Actual->animation == 1)
			couleurCourante(0,0,0);
		else if(Actual->animation == 0)
			couleurCourante(255,255,255);
					
		epaisseurDeTrait(2);
		ligne(278+tailleChaine(Game->P1.name,35),205,278+tailleChaine(Game->P1.name,35),255);
	}
	else if(Actual->chosing == 2) {		
		if(Actual->animation == 1)
			couleurCourante(0,0,0);
		else if(Actual->animation == 0)
			couleurCourante(255,255,255);
						
		epaisseurDeTrait(2);
		ligne(508+tailleChaine(Game->P2.name,35),205,508+tailleChaine(Game->P2.name,35),255);
	}
				
	// Shows if the names are "ok"
	if(strlen(Game->P1.name) > 0)
		displayPicture(Checked);
	else
		displayPicture(Unchecked);
}

/**
 * @brief      { Hides the pawns }
 *
 * @param      Game  pointer on gameParam struct
 */
void hidePawns(gameParam *Game)
{
	color c;
	c.r = c.g = c.b = 0;
	if(Game->P1.pos == 1) {
		couleurCourante(255,255,255);
		rectangle(450,450,900,520);
		frameButton(450,450,900,520,2,c);
	}
	else {
		couleurCourante(255,255,255);
		rectangle(450,80,900,150);
		frameButton(450,80,900,150,2,c);
	}
}

/**
 * @brief      {  Displays the background of the menus }
 */
void displayBackMenu(void)
{
	displayPicture(Bckgnd);
	displayPicture(Off);
}

/**
 * @brief      { Display the image called in parameters at the coords specified in the "Image" struct }
 *
 * @param[in]  img   Image struct
 */
void displayPicture(Image img)
{
	couleur bg;

	bg.r = 252;
	bg.v = 0;
	bg.b = 144;

	// With background
	if(img.type == 0) {
		ecrisImage(img.pos.x,img.pos.y,(img.donnees)->largeurImage,(img.donnees)->hauteurImage,(img.donnees)->donneesRGB);
	}
	// Without background
	else if(img.type == 1) {
		ecrisImageSansFond(img.pos.x,img.pos.y,(img.donnees)->largeurImage,(img.donnees)->hauteurImage,(img.donnees)->donneesRGB,bg);
	}
	// None of the 2 previous types
	else
		printf("\nProbleme d'image\n");
}

/**
 * @brief      { Frame the image called in parameters }
 *
 * @param[in]  img   Image struct
 */
void framePicture(Image img)
{
	int MARGIN = 5;
	float e = 2.5;	// Thickness
	color c;

	c.r = 200;
	c.g = 0;
	c.b = 0;
	frameButton(img.pos.x - MARGIN,img.pos.y - MARGIN, img.pos.x + (img.donnees)->largeurImage + MARGIN, img.pos.y + (img.donnees)->hauteurImage + MARGIN, e, c);
}

/**
 * @brief      { Frame any rectangle drawed by the GfxLib program }
 *
 * @param[in]  xmin  The xmin
 * @param[in]  ymin  The ymin
 * @param[in]  xmax  The xmax
 * @param[in]  ymax  The ymax
 * @param[in]  e     { Thickness }
 * @param[in]  c     { color struct }
 */
void frameButton(float xmin,float ymin, float xmax, float ymax, int e, color c)
{
	couleurCourante(c.r,c.g,c.b);
	epaisseurDeTrait(e);

	// Outsider lines
	ligne(xmin,ymin,xmax,ymin);
	ligne(xmax,ymin,xmax,ymax);
	ligne(xmax,ymax,xmin,ymax);
	ligne(xmin,ymax,xmin,ymin);
}

/**
 * @brief      { Frame the pawn of the box [i,j] }
 *
 * @param      Board  pointer on boardParam struct
 * @param[in]  i      { coordinate i }
 * @param[in]  j      { coordinate j }
 */
void framePawns(boardParam *Board, int i, int j)
{
	couleurCourante(155,255,0);
	epaisseurDeTrait(2);

	// Outsider lines
	ligne(Board->B[i][j].c.min.x,Board->B[i][j].c.min.y,Board->B[i][j].c.max.x,Board->B[i][j].c.min.y);
	ligne(Board->B[i][j].c.max.x,Board->B[i][j].c.min.y,Board->B[i][j].c.max.x,Board->B[i][j].c.max.y);
	ligne(Board->B[i][j].c.max.x,Board->B[i][j].c.max.y,Board->B[i][j].c.min.x,Board->B[i][j].c.max.y);
	ligne(Board->B[i][j].c.min.x,Board->B[i][j].c.max.y,Board->B[i][j].c.min.x,Board->B[i][j].c.min.y);
}

/**
 * @brief      { Frame the cage with a red rectangle }
 *
 * @param      Board  pointer on boardParam struct
 */
void frameCage(boardParam *Board)
{
	couleurCourante(255,0,0);
	epaisseurDeTrait(2);

	// Outsider lines
	for(int i = 0; i < 2; i++)
	{
	ligne(Board->BCage[i].c.min.x,Board->BCage[i].c.min.y,Board->BCage[i].c.max.x,Board->BCage[i].c.min.y);
	ligne(Board->BCage[i].c.max.x,Board->BCage[i].c.min.y,Board->BCage[i].c.max.x,Board->BCage[i].c.max.y);
	ligne(Board->BCage[i].c.max.x,Board->BCage[i].c.max.y,Board->BCage[i].c.min.x,Board->BCage[i].c.max.y);
	ligne(Board->BCage[i].c.min.x,Board->BCage[i].c.max.y,Board->BCage[i].c.min.x,Board->BCage[i].c.min.y);
	}
	
}

/**
 * @brief      { Display Selected pawn}
 *
 * @param      Board  pointer on boardParam struct
 */
void displaySelectedPawns(boardParam *Board)
{
	couleurCourante(255,0,0);
	epaisseurDeTrait(2);
	if(Board->pawnSelect.x != -1 && Board->pawnSelect.y != -1)
		framePawns(Board,Board->pawnSelect.x,Board->pawnSelect.y);
}

/**
 * @brief      { Display the end of the game }
 *
 * @param      Game  pointer on gameParam struct
 */
void displayEnd(gameParam *Game)
{
	
	displayPicture(EndOff);
	displayPicture(PrincipalMenu);
	displayPicture(Restart);
	
	displayWinner(Game);
	

	couleurCourante(69,39,13);
	epaisseurDeTrait(3);
	afficheChaine("Fin de la partie",25,100,400);


}

/**
 * @brief      { Return who has won }
 *
 * @param      Game    pointer on gameParam struct
 * @param      winner  The winner
 */
void renvoiGagnant(gameParam *Game,char *winner)
{
	// si un joueur gagne
	if(Game->P1.win ==  1 && Game->P1.type == 0){
		strcpy(winner,Game->P1.name);
	}
	else if(Game->P2.win ==  1 && Game->P2.type == 0){
		strcpy(winner,Game->P2.name);
	}
	// si deux IA s'affrontent
	else if(Game->P1.win ==  1 && Game->P1.type == 1 && Game->P2.type == 1)
	{
		strcpy(winner,"IA 1");
	}
	else if(Game->P2.win ==  1 && Game->P1.type == 1 && Game->P2.type == 1)
	{
		strcpy(winner,"IA 2");
	}
	// si l'IA gagne contre un joueur
	else if((Game->P1.win ==  1 && Game->P1.type == 1 && Game->P2.type == 0)||
			 (Game->P2.win ==  1 && Game->P2.type == 1 && Game->P1.type == 0))
	{
		strcpy(winner,"IA");
	}
}

/**
 * @brief      { Display the winner }
 *
 * @param      Game  pointer on gameParam struct
 */
void displayWinner(gameParam *Game)
{
	char winner[PSEUDO] = " ";
	char chaine[60];

	renvoiGagnant(Game, winner);

	couleurCourante(69,39,13);
	epaisseurDeTrait(1);
	
	strcpy(chaine,"WINNER : ");
	strcat(chaine,winner);
	afficheChaine(chaine,16,105,350);
}