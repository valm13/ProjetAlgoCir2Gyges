#ifndef AFFICHAGE_H
#define AFFICHAGE_H

extern Image Rules;
extern Image Play;
extern Image Back;
extern Image Off;
extern Image North;
extern Image South;
extern Image Begin;

extern Image HumanvsHuman;
extern Image HumanvsIA;
extern Image IAvsIA;

extern Image SimpleP;
extern Image DoubleP;
extern Image TripleP;

extern Image Bckgnd;
extern Image BckgndGame;
extern Image NameP1;
extern Image NameP2;
extern Image LocationP1;
extern Image Opponent;
extern Image RulesP1;
extern Image RulesP2;
extern Image RulesP3;
extern Image RulesP4;

extern Image Checked;
extern Image Unchecked;

extern Image LeftEnable;
extern Image LeftDisable;
extern Image RightEnable;
extern Image RightDisable;

extern Image PrincipalMenu;
extern Image EndOff;
extern Image Restart;


typedef struct color
{
	int r,g,b;
}color;

void manageParam(gameParam *Game, Now *Actual);

void imageInitialisation(void);
	void loadImage(Image *Img, short x, short y, short t, char C[50]);
void releaseImages(void);

void manageDisplay(gameParam *Game, boardParam *Board, Now *Actual);
	void displayMenu(gameParam *Game, Now *Actual);
		void displayRules(gameParam *Game);
			void directionButton(gameParam *Game);
		void displayMainMenu(void);
		void displayGameType(gameParam *Game);
			void selectionType(gameParam *Game);
		void manageDisplayName(gameParam *Game,Now *Actual);
			void displayNameMulti(gameParam *Game, Now *Actual);
			void displayNameSolo(gameParam *Game, Now *Actual);
			// void displayNameIA(gameParam *Game);
		void displayPlacementChoice(gameParam *Game);
			void selectionPlacementChoice(gameParam *Game);
	void displayGame(gameParam *Game, boardParam *Board);
	void displayPawns(boardParam *Board);
	void displayPlayablePlaces(boardParam *Board);
	void hidePawns(gameParam *Game);

void displayPicture(Image img);
void displayBackMenu(void);

void frameButton(float xmin,float ymin, float xmax, float ymax, int e, color c);
void framePicture(Image img);


void framePawns(boardParam *Board, int i, int j);
void frameCage(boardParam *Board);
void displaySelectedPawns(boardParam *Board);
void displayEnd(gameParam *Game);
void displayWinner(gameParam *Game);
void renvoiGagnant(gameParam *Game, char *winner);
#endif