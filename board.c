#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libISEN/GfxLib.h"
#include "libISEN/BmpLib.h"
#include "structure.h"
#include "GestionAffichage.h"
#include "GereClic.h"
#include "GereClavier.h"
#include "board.h"

/**
 * \brief      { Initialize the gameboard }
 *
 * \param      Board  pointer on boardParam struct
 */
void initBoard(boardParam *Board)
{
	int i,j;

	Board->P1placement = 0;
	Board->P2placement = 0;

	for(i = 0; i < BOARD_DIM; i++) {
		for(j = 0; j < BOARD_DIM; j++) {
			Board->B[i][j].c.min.x = 455 + (OFFSET * j);
			Board->B[i][j].c.min.y = 80 + (OFFSET * i);
			Board->B[i][j].c.max.x = 520 + (OFFSET * j);
			Board->B[i][j].c.max.y = 145 + (OFFSET * i);
			Board->B[i][j].state = 0;
			Board->B[i][j].end = 0;
			Board->B[i][j].parent.up = 
			Board->B[i][j].parent.right = 
			Board->B[i][j].parent.down = 
			Board->B[i][j].parent.left = 
			Board->B[i][j].parent.root = false;
		}
	}
	for(i = 0; i < 2; i++) {
		Board->BCage[i].c.min.x = 640;
		Board->BCage[i].c.max.x = 709;
		Board->BCage[i].c.min.y = 3 + 525*i;
		Board->BCage[i].c.max.y = 72 + 525*i;
		Board->BCage[i].state = 0;
	}
}

/**
 * @brief      { Reset places of everybox of the board }
 *
 * @param      Board  pointer on boardParam struct
 */
void resetPlaces(boardParam *Board)
{
	int i,j;

	for(i = 0; i < BOARD_DIM; i++) {
		for(j = 0; j < BOARD_DIM; j++) {
			Board->B[i][j].end = 0;
			resetParent(Board, i, j);			
		}
	}
	for(i = 0; i < 2; i++) {
		Board->BCage[i].end = 0;
	}
}

/**
 * @brief      { function_description }
 *
 * @param      Game       pointer on gameParam struct
 * @param      Board      pointer on boardParam struct
 * @param[in]  x          { coordinate x }
 * @param[in]  y          { coordinate y }
 * @param      state      The state of the box
 * @param      path       The path
 * @param[in]  direction  The direction
 */
void manip(gameParam *Game, boardParam *Board, int x, int y, short *state, short *path,  int direction)
{
	recursiveIn(state, path);
	setParent(Board, x, y, direction);
	calcMove(Game, Board, x, y, state, path);
	recursiveOut(state);
}

/**
 * @brief      { Verify if the coordinates are in the board }
 *
 * @param[in]  x     { coordinate x }
 * @param[in]  y     { coordinate y }
 *
 * @return     { description_of_the_return_value }
 */
bool checkBoardLimits(int x, int y)
{
	if((x >= 0 && x < BOARD_DIM) && (y >= 0 && y < BOARD_DIM))
		return true;
	else
		return false;
}

/**
 * @brief      { Check if there is a pawn }
 *
 * @param      Board  pointer on boardParam struct
 * @param[in]  x      { coordinate x }
 * @param[in]  y      { coordinate y }
 *
 * @return     { true if there is no pawns }
 */
bool checkValue(boardParam *Board, int x, int y)
{
	if(Board->B[x][y].state > 0)
		return false;
	else
		return true;
}

/**
 * @brief      { Sets the root }
 *
 * @param      Board  pointer on boardParam struct
 * @param[in]  x      { coordinate x }
 * @param[in]  y      { coordinate y }
 */
void setRoot(boardParam *Board, int x, int y)
{
	Board->B[x][y].parent.root = true;
}

/**
 * @brief      { Search the parent of a box }
 *
 * @param      Board  pointer on boardParam struct
 * @param[in]  x      { coordinate x }
 * @param[in]  y      { coordinate y }
 *
 * @return     { description_of_the_return_value }
 */
int searchParent(boardParam *Board, int x, int y)
{
	if(Board->B[x][y].parent.up)
		return 1;
	else if(Board->B[x][y].parent.right)
		return 2;
	else if(Board->B[x][y].parent.down)
		return 3;
	else if(Board->B[x][y].parent.left)
		return 4;
	else if(Board->B[x][y].parent.root)
		return 0;
}

/**
 * @brief      { Sets the parent of a box }
 *
 * @param      Board      pointer on boardParam struct
 * @param[in]  x          { coordinate x }
 * @param[in]  y          { coordinate y }
 * @param[in]  direction  The direction
 */
void setParent(boardParam *Board, int x, int y, int direction)
{
	if(Board->B[x][y].parent.root == true) {
		return;
	}
	else {
		switch(direction) {
			case 1:
				resetParent(Board, x, y);
				Board->B[x][y].parent.up = true;
			break;

			case 2:
				resetParent(Board, x, y);
				Board->B[x][y].parent.right = true;
			break;

			case 3:
				resetParent(Board, x, y);
				Board->B[x][y].parent.down = true;
			break;

			case 4:
				resetParent(Board, x, y);
				Board->B[x][y].parent.left = true;
			break;
		}
	}
}

/**
 * @brief      { reset parent of a box }
 *
 * @param      Board  pointer on boardParam struct
 * @param[in]  x      { coordinate x }
 * @param[in]  y      { coordinate y }
 */
void resetParent(boardParam *Board, int x, int y)
{
	Board->B[x][y].parent.up = 
	Board->B[x][y].parent.right = 
	Board->B[x][y].parent.down = 
	Board->B[x][y].parent.left = 
	Board->B[x][y].parent.root = false;
}

/**
 * @brief      { function_description }
 *
 * @param      state  The state of the box
 * @param      path   The path
 */
void recursiveIn(short *state, short *path)
{
	(*state)--;
	(*path)++;

}

/**
 * @brief      { function_description }
 *
 * @param      state  The state of the box
 */
void recursiveOut(short *state)
{
	(*state)++;
}

/**
 * @brief      { Calculates the move }
 *
 * @param      Game   pointer on gameParam struct
 * @param      Board  pointer on boardParam struct
 * @param[in]  x      { parameter_description }
 * @param[in]  y      { parameter_description }
 * @param      state  The state of the box
 * @param      path   The path
 */
void calcMove(gameParam *Game, boardParam *Board, int x, int y, short *state, short *path) {
	int parent = searchParent(Board, x, y);

	checkWinnable(Game, Board, x, state);

	if((*path) > 100)
		return;

	if((*state) > 0) {
		switch(parent) {
			case 0:
				if(checkBoardLimits(x+1, y)) {
					if((*state == 1) || (checkValue(Board, x+1, y)))
						manip(Game, Board, x+1, y, state, path, 1);
				}
				else {

				}
				if(checkBoardLimits(x, y+1)) {
					if((*state == 1) || (checkValue(Board, x, y+1)))
						manip(Game, Board, x, y+1, state, path, 2);
				}
				if(checkBoardLimits(x-1, y)) {
					if((*state == 1) || (checkValue(Board, x-1, y)))
						manip(Game, Board, x-1, y, state, path, 3);
				}
				if(checkBoardLimits(x, y-1)) {
					if((*state == 1) || (checkValue(Board, x, y-1)))
						manip(Game, Board, x, y-1, state, path, 4);
				}
			break;

			case 1:
				if(checkBoardLimits(x+1, y)) {
					if((*state == 1) || (checkValue(Board, x+1, y)))
						manip(Game, Board, x+1, y, state, path, 1);
				}
				if(checkBoardLimits(x, y+1)) {
					if((*state == 1) || (checkValue(Board, x, y+1)))
						manip(Game, Board, x, y+1, state, path, 2);
				}
				if(checkBoardLimits(x, y-1)) {
					if((*state == 1) || (checkValue(Board, x, y-1)))
						manip(Game, Board, x, y-1, state, path, 4);
				}
			break;

			case 2:
				if(checkBoardLimits(x+1, y)) {
					if((*state == 1) || (checkValue(Board, x+1, y)))
						manip(Game, Board, x+1, y, state, path, 1);
				}
				if(checkBoardLimits(x, y+1)) {
					if((*state == 1) || (checkValue(Board, x, y+1)))
						manip(Game, Board, x, y+1, state, path, 2);
				}
				if(checkBoardLimits(x-1, y)) {
					if((*state == 1) || (checkValue(Board, x-1, y)))
						manip(Game, Board, x-1, y, state, path, 3);
				}
			break;

			case 3:
				if(checkBoardLimits(x, y+1)) {
					if((*state == 1) || (checkValue(Board, x, y+1)))
						manip(Game, Board, x, y+1, state, path, 2);
				}
				if(checkBoardLimits(x-1, y)) {
					if((*state == 1) || (checkValue(Board, x-1, y)))
						manip(Game, Board, x-1, y, state, path, 3);
				}
				if(checkBoardLimits(x, y-1)) {
					if((*state == 1) || (checkValue(Board, x, y-1)))
						manip(Game, Board, x, y-1, state, path, 4);
				}
			break;

			case 4:
				if(checkBoardLimits(x+1, y)) {
					if((*state == 1) || (checkValue(Board, x+1, y)))
						manip(Game, Board, x+1, y, state, path, 1);
				}
				if(checkBoardLimits(x-1, y)) {
					if((*state == 1) || (checkValue(Board, x-1, y)))
						manip(Game, Board, x-1, y, state, path, 3);
				}
				if(checkBoardLimits(x, y-1)) {
					if((*state == 1) || (checkValue(Board, x, y-1)))
						manip(Game, Board, x, y-1, state, path, 4);
				}
			break;
		}
	}

	else if((*state) == 0) {
		if(Board->B[x][y].state > 0) {
			if(Board->B[x][y].parent.root)
				return;
			else {
				short tempState = Board->B[x][y].state;
				calcMove(Game, Board, x, y, &tempState, path);
			}
		}
		else
			Board->B[x][y].end = 1;
		return;
	}
}

/**
 * @brief      { function_description }
 *
 * @param      Game   pointer on gameParam struct
 * @param      Board  pointer on boardParam struct
 * @param[in]  x      { parameter_description }
 * @param      state  The state of the box
 */
void checkWinnable(gameParam *Game, boardParam *Board, int x, short *state)
{
	if((*state) == 1) {
		if(Game->gameStatus.whoPlays == 1) {
			if(Game->P1.pos == 1) {
				if(x == 0){
					Board->BCage[0].end = 1;

				}
			}
			else {
				if(x == BOARD_DIM-1) {
					Board->BCage[1].end = 1;
					
				}
			}
		}
		else {
			if(Game->P2.pos == 1) {
				if(x == 0){
					Board->BCage[0].end = 1;
					
				}
			}
			else {
				if(x == BOARD_DIM-1) {
					Board->BCage[1].end = 1;
					
				}
			}
		}
	}
	else
		return;
}