#ifndef BOARD_H
#define BOARD_H

/**
 * @brief      { Initialize the gameboard }
 *
 * @param      Board  pointer on boardParam struct
 */
void initBoard(boardParam *Board);

void setRoot(boardParam *Board, int x, int y);
/**
 * @brief      { Reset places of everybox of the board }
 *
 * @param      Board  pointer on boardParam struct
 */
void resetPlaces(boardParam *Board);
void manip(gameParam *Game, boardParam *Board, int x, int y, short *state, short *path, int direction);

bool checkBoardLimits(int x, int y);
bool checkValue(boardParam *Board, int x, int y);


int searchParent(boardParam *Board, int x, int y);
void setParent(boardParam *Board, int x, int y, int direction);
void resetParent(boardParam *Board, int x, int y);


void recursiveIn(short *state, short *path);
void recursiveOut(short *state);

void calcMove(gameParam *Game, boardParam *Board, int x, int y, short *state, short *path);

void checkWinnable(gameParam *Game, boardParam *Board, int x, short *state);

#endif