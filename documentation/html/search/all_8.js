var searchData=
[
  ['iavsia',['IAvsIA',['../_gestion_affichage_8c.html#aa8625a5d002cd1400a208537e57c928a',1,'IAvsIA():&#160;GestionAffichage.c'],['../_gestion_affichage_8h.html#aa8625a5d002cd1400a208537e57c928a',1,'IAvsIA():&#160;GestionAffichage.c']]],
  ['image',['Image',['../struct_image.html',1,'Image'],['../structure_8h.html#acf12205a65321baefe5174db248f56f6',1,'Image():&#160;structure.h']]],
  ['imageinitialisation',['imageInitialisation',['../_gestion_affichage_8c.html#a199e29bf12cadc9eaf7b5825890103a0',1,'imageInitialisation(void):&#160;GestionAffichage.c'],['../_gestion_affichage_8h.html#a199e29bf12cadc9eaf7b5825890103a0',1,'imageInitialisation(void):&#160;GestionAffichage.c']]],
  ['initboard',['initBoard',['../board_8c.html#aa26d6d84e457b8103e70cc98a75a40a4',1,'initBoard(boardParam *Board):&#160;board.c'],['../board_8h.html#aa26d6d84e457b8103e70cc98a75a40a4',1,'initBoard(boardParam *Board):&#160;board.c']]],
  ['isthereapawnselected',['isThereAPawnSelected',['../_gere_clic_8c.html#aa2691fabc2bb0e021d5ec1d20af44316',1,'isThereAPawnSelected(boardParam *Board):&#160;GereClic.c'],['../_gere_clic_8h.html#aa2691fabc2bb0e021d5ec1d20af44316',1,'isThereAPawnSelected(boardParam *Board):&#160;GereClic.c']]]
];
