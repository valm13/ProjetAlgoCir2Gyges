var searchData=
[
  ['r',['r',['../structcolor.html#acab531abaa74a7e664e3986f2522b33a',1,'color']]],
  ['recursivein',['recursiveIn',['../board_8c.html#a435abcfcd42fd1193d8d1c2a49ba70fe',1,'recursiveIn(short *state, short *path):&#160;board.c'],['../board_8h.html#a435abcfcd42fd1193d8d1c2a49ba70fe',1,'recursiveIn(short *state, short *path):&#160;board.c']]],
  ['recursiveout',['recursiveOut',['../board_8c.html#ac0555a7d8443ea5225ff23adc69c2628',1,'recursiveOut(short *state):&#160;board.c'],['../board_8h.html#ac0555a7d8443ea5225ff23adc69c2628',1,'recursiveOut(short *state):&#160;board.c']]],
  ['releaseimages',['releaseImages',['../_gestion_affichage_8c.html#a702fb33b62e6ca1ef45a3d5317505d04',1,'releaseImages(void):&#160;GestionAffichage.c'],['../_gestion_affichage_8h.html#a702fb33b62e6ca1ef45a3d5317505d04',1,'releaseImages(void):&#160;GestionAffichage.c']]],
  ['renvoigagnant',['renvoiGagnant',['../_gestion_affichage_8c.html#ae7e116c374dc694da7235c1d6574803d',1,'renvoiGagnant(gameParam *Game, char *winner):&#160;GestionAffichage.c'],['../_gestion_affichage_8h.html#ae7e116c374dc694da7235c1d6574803d',1,'renvoiGagnant(gameParam *Game, char *winner):&#160;GestionAffichage.c']]],
  ['resetparent',['resetParent',['../board_8c.html#a20d823f4044098683a231b2456af0b0a',1,'resetParent(boardParam *Board, int x, int y):&#160;board.c'],['../board_8h.html#a20d823f4044098683a231b2456af0b0a',1,'resetParent(boardParam *Board, int x, int y):&#160;board.c']]],
  ['resetplaces',['resetPlaces',['../board_8c.html#ae30721e89bb56a9c847ad7483b468be1',1,'resetPlaces(boardParam *Board):&#160;board.c'],['../board_8h.html#ae30721e89bb56a9c847ad7483b468be1',1,'resetPlaces(boardParam *Board):&#160;board.c']]],
  ['restart',['Restart',['../_gestion_affichage_8c.html#a34ab2b704ef360e56f04bf09a15d2738',1,'Restart():&#160;GestionAffichage.c'],['../_gestion_affichage_8h.html#a34ab2b704ef360e56f04bf09a15d2738',1,'Restart():&#160;GestionAffichage.c']]],
  ['restartgame',['restartGame',['../_gere_clic_8c.html#a91356f153e642e1f0f4061821aefe0d8',1,'restartGame(gameParam *Game, boardParam *Board, Now *Actual):&#160;GereClic.c'],['../_gere_clic_8h.html#a91356f153e642e1f0f4061821aefe0d8',1,'restartGame(gameParam *Game, boardParam *Board, Now *Actual):&#160;GereClic.c']]],
  ['right',['right',['../structdirection.html#acd047173db926d54ee91589f7ab53492',1,'direction']]],
  ['rightdisable',['RightDisable',['../_gestion_affichage_8c.html#a473d29148e41b764a352347da6d1e5d2',1,'RightDisable():&#160;GestionAffichage.c'],['../_gestion_affichage_8h.html#a473d29148e41b764a352347da6d1e5d2',1,'RightDisable():&#160;GestionAffichage.c']]],
  ['rightenable',['RightEnable',['../_gestion_affichage_8c.html#ac73f35e632f3f036c3d824172de65750',1,'RightEnable():&#160;GestionAffichage.c'],['../_gestion_affichage_8h.html#ac73f35e632f3f036c3d824172de65750',1,'RightEnable():&#160;GestionAffichage.c']]],
  ['root',['root',['../structdirection.html#a7802af2b886054d239fe256dec029c23',1,'direction']]],
  ['rules',['Rules',['../_gestion_affichage_8c.html#a90d73bc24c8c38c73ad122704605f4a9',1,'Rules():&#160;GestionAffichage.c'],['../_gestion_affichage_8h.html#a90d73bc24c8c38c73ad122704605f4a9',1,'Rules():&#160;GestionAffichage.c']]],
  ['rulesp1',['RulesP1',['../_gestion_affichage_8c.html#aebbc092e691eeebfae12b047bec2cda8',1,'RulesP1():&#160;GestionAffichage.c'],['../_gestion_affichage_8h.html#aebbc092e691eeebfae12b047bec2cda8',1,'RulesP1():&#160;GestionAffichage.c']]],
  ['rulesp2',['RulesP2',['../_gestion_affichage_8c.html#a62c55771fbfc138f979dcabe31a3e090',1,'RulesP2():&#160;GestionAffichage.c'],['../_gestion_affichage_8h.html#a62c55771fbfc138f979dcabe31a3e090',1,'RulesP2():&#160;GestionAffichage.c']]],
  ['rulesp3',['RulesP3',['../_gestion_affichage_8c.html#a0157b4a25694129ab18e24d687732e28',1,'RulesP3():&#160;GestionAffichage.c'],['../_gestion_affichage_8h.html#a0157b4a25694129ab18e24d687732e28',1,'RulesP3():&#160;GestionAffichage.c']]],
  ['rulesp4',['RulesP4',['../_gestion_affichage_8c.html#aef0ef2eb237135afbff08bf8ec7a1107',1,'RulesP4():&#160;GestionAffichage.c'],['../_gestion_affichage_8h.html#aef0ef2eb237135afbff08bf8ec7a1107',1,'RulesP4():&#160;GestionAffichage.c']]]
];
