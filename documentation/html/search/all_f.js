var searchData=
[
  ['searchparent',['searchParent',['../board_8c.html#a1d629f088de1e0f70f81147a7ab99dc3',1,'searchParent(boardParam *Board, int x, int y):&#160;board.c'],['../board_8h.html#a1d629f088de1e0f70f81147a7ab99dc3',1,'searchParent(boardParam *Board, int x, int y):&#160;board.c']]],
  ['selectionplacementchoice',['selectionPlacementChoice',['../_gestion_affichage_8c.html#a3cb3359eb0463188ca8363254a3d341b',1,'selectionPlacementChoice(gameParam *Game):&#160;GestionAffichage.c'],['../_gestion_affichage_8h.html#a3cb3359eb0463188ca8363254a3d341b',1,'selectionPlacementChoice(gameParam *Game):&#160;GestionAffichage.c']]],
  ['selectiontype',['selectionType',['../_gestion_affichage_8c.html#aed9dc63d5794942388018306e73a695a',1,'selectionType(gameParam *Game):&#160;GestionAffichage.c'],['../_gestion_affichage_8h.html#aed9dc63d5794942388018306e73a695a',1,'selectionType(gameParam *Game):&#160;GestionAffichage.c']]],
  ['selectpawn',['selectPawn',['../_gere_clic_8c.html#a2738d1b51633db63ea285b7a800b043c',1,'selectPawn(gameParam *Game, boardParam *Board):&#160;GereClic.c'],['../_gere_clic_8h.html#a2738d1b51633db63ea285b7a800b043c',1,'selectPawn(gameParam *Game, boardParam *Board):&#160;GereClic.c']]],
  ['setparent',['setParent',['../board_8c.html#a5ca346519d476045f3fa72f28f5fb3c3',1,'setParent(boardParam *Board, int x, int y, int direction):&#160;board.c'],['../board_8h.html#a5ca346519d476045f3fa72f28f5fb3c3',1,'setParent(boardParam *Board, int x, int y, int direction):&#160;board.c']]],
  ['setroot',['setRoot',['../board_8c.html#a06f4e1b5c78356d1f1ce5cda3c76cb66',1,'setRoot(boardParam *Board, int x, int y):&#160;board.c'],['../board_8h.html#a06f4e1b5c78356d1f1ce5cda3c76cb66',1,'setRoot(boardParam *Board, int x, int y):&#160;board.c']]],
  ['simplep',['SimpleP',['../_gestion_affichage_8c.html#a58ab16f03121d14cd9a436179e2ee93b',1,'SimpleP():&#160;GestionAffichage.c'],['../_gestion_affichage_8h.html#a58ab16f03121d14cd9a436179e2ee93b',1,'SimpleP():&#160;GestionAffichage.c']]],
  ['south',['South',['../_gestion_affichage_8c.html#a4fd1b96773295c2c2351d82a00ad93cf',1,'South():&#160;GestionAffichage.c'],['../_gestion_affichage_8h.html#a4fd1b96773295c2c2351d82a00ad93cf',1,'South():&#160;GestionAffichage.c']]],
  ['square',['square',['../structsquare.html',1,'square'],['../structure_8h.html#ac0e5faa55b5239564de85340a1abe860',1,'square():&#160;structure.h']]],
  ['state',['state',['../structbox.html#ac312388b301cc0ab92a54c52f2f1dc6e',1,'box']]],
  ['status',['status',['../structstatus.html',1,'status'],['../structure_8h.html#abe68fdb99ba24ae9b96f8b7841679c6b',1,'status():&#160;structure.h']]],
  ['structure_2eh',['structure.h',['../structure_8h.html',1,'']]],
  ['switchplayerturn',['switchPlayerTurn',['../_gere_clic_8c.html#a80e926725fe6e79be5bc2946ae723a3d',1,'switchPlayerTurn(gameParam *Game):&#160;GereClic.c'],['../_gere_clic_8h.html#a80e926725fe6e79be5bc2946ae723a3d',1,'switchPlayerTurn(gameParam *Game):&#160;GereClic.c']]]
];
