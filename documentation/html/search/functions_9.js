var searchData=
[
  ['recursivein',['recursiveIn',['../board_8c.html#a435abcfcd42fd1193d8d1c2a49ba70fe',1,'recursiveIn(short *state, short *path):&#160;board.c'],['../board_8h.html#a435abcfcd42fd1193d8d1c2a49ba70fe',1,'recursiveIn(short *state, short *path):&#160;board.c']]],
  ['recursiveout',['recursiveOut',['../board_8c.html#ac0555a7d8443ea5225ff23adc69c2628',1,'recursiveOut(short *state):&#160;board.c'],['../board_8h.html#ac0555a7d8443ea5225ff23adc69c2628',1,'recursiveOut(short *state):&#160;board.c']]],
  ['releaseimages',['releaseImages',['../_gestion_affichage_8c.html#a702fb33b62e6ca1ef45a3d5317505d04',1,'releaseImages(void):&#160;GestionAffichage.c'],['../_gestion_affichage_8h.html#a702fb33b62e6ca1ef45a3d5317505d04',1,'releaseImages(void):&#160;GestionAffichage.c']]],
  ['renvoigagnant',['renvoiGagnant',['../_gestion_affichage_8c.html#ae7e116c374dc694da7235c1d6574803d',1,'renvoiGagnant(gameParam *Game, char *winner):&#160;GestionAffichage.c'],['../_gestion_affichage_8h.html#ae7e116c374dc694da7235c1d6574803d',1,'renvoiGagnant(gameParam *Game, char *winner):&#160;GestionAffichage.c']]],
  ['resetparent',['resetParent',['../board_8c.html#a20d823f4044098683a231b2456af0b0a',1,'resetParent(boardParam *Board, int x, int y):&#160;board.c'],['../board_8h.html#a20d823f4044098683a231b2456af0b0a',1,'resetParent(boardParam *Board, int x, int y):&#160;board.c']]],
  ['resetplaces',['resetPlaces',['../board_8c.html#ae30721e89bb56a9c847ad7483b468be1',1,'resetPlaces(boardParam *Board):&#160;board.c'],['../board_8h.html#ae30721e89bb56a9c847ad7483b468be1',1,'resetPlaces(boardParam *Board):&#160;board.c']]],
  ['restartgame',['restartGame',['../_gere_clic_8c.html#a91356f153e642e1f0f4061821aefe0d8',1,'restartGame(gameParam *Game, boardParam *Board, Now *Actual):&#160;GereClic.c'],['../_gere_clic_8h.html#a91356f153e642e1f0f4061821aefe0d8',1,'restartGame(gameParam *Game, boardParam *Board, Now *Actual):&#160;GereClic.c']]]
];
