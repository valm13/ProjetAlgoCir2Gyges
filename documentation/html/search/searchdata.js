var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstuwxy",
  1: "bcdginps",
  2: "bgmst",
  3: "cdfghilmprstw",
  4: "abcdeghilmnoprstuwxy",
  5: "bcdginps",
  6: "bhlnop"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Macros"
};

