var searchData=
[
  ['p1',['P1',['../structgame_param.html#a7424bb3c252d0ce4d9cf653dccca50b5',1,'gameParam']]],
  ['p1placement',['P1placement',['../structboard_param.html#a187102b7b7b32414a153bd248834a6ec',1,'boardParam']]],
  ['p2',['P2',['../structgame_param.html#a7f564e3a7b37b21163b48b181f5af7a0',1,'gameParam']]],
  ['p2placement',['P2placement',['../structboard_param.html#a99a773b1bfbe7441f87254682d7be23c',1,'boardParam']]],
  ['page',['page',['../structgame_param.html#a2629561507f44259d54c73a18dc4ec33',1,'gameParam']]],
  ['parent',['parent',['../structbox.html#ac87b6843bb02f7abf38dcf68e0d150e6',1,'box']]],
  ['pawnselect',['pawnSelect',['../structboard_param.html#a05a5718e606801e661cc6fd9a30edca5',1,'boardParam']]],
  ['place',['place',['../structgame_param.html#a389471186613148b107822101975ae0b',1,'gameParam']]],
  ['play',['Play',['../_gestion_affichage_8c.html#a4f53a7b8cd82d3af02b6b6209d3a5f59',1,'Play():&#160;GestionAffichage.c'],['../_gestion_affichage_8h.html#a4f53a7b8cd82d3af02b6b6209d3a5f59',1,'Play():&#160;GestionAffichage.c']]],
  ['playboard',['PlayBoard',['../_gestion_affichage_8c.html#a72e007539c9fb59dc5f29c2c5b7988a9',1,'GestionAffichage.c']]],
  ['pos',['pos',['../structpawn.html#a7ae7dc4ab00cce14153c6db9e7b7e185',1,'pawn::pos()'],['../structplayer.html#aef74ed06fc6ac8a9479ddb5bce449f2e',1,'player::pos()'],['../struct_image.html#a7ae7dc4ab00cce14153c6db9e7b7e185',1,'Image::pos()']]],
  ['principalmenu',['PrincipalMenu',['../_gestion_affichage_8c.html#a699d3556e312d5ba8bfee0a5e645368b',1,'PrincipalMenu():&#160;GestionAffichage.c'],['../_gestion_affichage_8h.html#a699d3556e312d5ba8bfee0a5e645368b',1,'PrincipalMenu():&#160;GestionAffichage.c']]]
];
