#include <stdlib.h> // Pour pouvoir utiliser exit()
#include <stdio.h> // Pour pouvoir utiliser printf()
#include <time.h>
#include <string.h>
#include "libISEN/GfxLib.h" // Seul cet include est necessaire pour faire du graphique
#include "libISEN/BmpLib.h" // Cet include permet de manipuler des fichiers BMP
#include "structure.h"
#include "GestionAffichage.h" // Pour gérer l'affichage
#include "GereClic.h" // Pour gérer les clics
#include "GereClavier.h" // Pour gérer les clics des touches du clavier
#include "board.h"
#include "toolbox.h" // Pour utiliser des fonctions pouvant être génériques

// Largeur et hauteur de la fenêtre
#define LargeurFenetre 900
#define HauteurFenetre 600

/* La fonction de gestion des evenements, appelee automatiquement par le systeme
des qu'une evenement survient */
void gestionEvenement(EvenementGfx evenement);



int main(int argc, char **argv)
{
	initialiseGfx(argc, argv);
	srand(time(NULL));
	
	prepareFenetreGraphique("Gyges, the board game !", LargeurFenetre, HauteurFenetre);
	
	/* Lance la boucle qui aiguille les evenements sur la fonction gestionEvenement ci-apres,
		qui elle-meme utilise fonctionAffichage ci-dessous */
	lanceBoucleEvenements();
	
	return 0;
}

/* La fonction de gestion des evenements, appelee automatiquement par le systeme
des qu'une evenement survient */
void gestionEvenement(EvenementGfx evenement)
{
	static gameParam Game;
	static boardParam Board;
	static Now Actual;

	switch (evenement)
	{
		case Initialisation:
			imageInitialisation();
			initBoard(&Board);
			gameInitialisation(&Game, &Actual, &Board);
			demandeTemporisation(-1);
		break;
		
		case Temporisation:
			TempoTextField(&Game, &Actual);
			rafraichisFenetre();
		break;
			
		case Affichage:
			effaceFenetre(255,255,255);
			manageDisplay(&Game, &Board, &Actual);
		break;
			
		case Clavier:
			printf("ASCII %d\n", caractereClavier());
			if(caractereClavier() == 42)
			{
				// Pour avoir un apercu des donnees en appuyant sur la touche *  
				DisplayGameData(&Game);

			} 
			manageNormalKeyboardMenu(&Game, &Actual, caractereClavier());

			rafraichisFenetre();
		break;
			
		case ClavierSpecial:
			printf("Special ASCII %d\n", toucheClavier());
			manageSpecialKeyboardMenu(&Game,toucheClavier());
			rafraichisFenetre();
		break;

		case BoutonSouris:
			if (etatBoutonSouris() == GaucheAppuye)
			{
				printf("Clic gauche : abscisseSouris = %d ordonneeSouris = %d \n",abscisseSouris(),ordonneeSouris());
				
				manageClic(&Game, &Board, &Actual);
			}

			if(etatBoutonSouris() == DroiteAppuye)
			{
				printf("Clic droit\n");
				
			}

			rafraichisFenetre();
		break;
		
		case Souris: // Si la souris est deplacee
		break;
		
		case Inactivite: // Quand aucun message n'est disponible
		break;
		
		case Redimensionnement: 
			printf("Largeur : %d\t", largeurFenetre());
			printf("Hauteur : %d\n", hauteurFenetre());
		break;
	}
}
