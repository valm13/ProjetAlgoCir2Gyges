#ifndef STRUCT_H
#define STRUCT_H
#include "libISEN/BmpLib.h"

// Length of char "names"
#define PSEUDO 15
// Number of pawns
#define NB_PAWNS 12
// Board dimensions
#define BOARD_DIM 6

#define OFFSET 75

typedef struct dot
{
	int x,y;
}dot;

typedef struct square{
	dot min, max;
}square;

typedef struct direction {
	bool up, down, left, right;
	bool root;
}direction;

typedef struct box {
	square c;
	direction parent;
	short state;
	short end;
}box;

typedef struct pawn{
	short type;
	dot pos;
}pawn;

typedef struct boardParam{
	short P1placement;
	short P2placement;
	box B[BOARD_DIM][BOARD_DIM];
	box BCage[2]; // cases d'arrivé 0 : sud  et 1 : nord
	dot pawnSelect; // unselected
}boardParam;

/**
 * struct of a player
 * @param      name    		 name of the player
 * @param      position 	 position on the game board : North = 1 | South = 2
 * @param      win    		 player has won : Yes = 1 | No = 0
 */
typedef struct player{
	char name[PSEUDO];
	short pos;
	short win;
	short type;	// 0 : humain 1 : I.A
}player;

typedef struct status{
	short action;	//	0 : partie pas commencée | 1 : placement des pions | 2 : partie commencée
	short whoPlays;	// alterne entre 1 et 2  (le premier à jouer est le personnage en pos =  2 (SUD))
}status;

typedef struct gameParam{
	status gameStatus;
	short place;
	short page;
	short type;
	player P1;
	player P2;
}gameParam;

typedef struct Image{
	DonneesImageRGB *donnees;
	short	type;	//0 : ArrierePlan	1:imageSansfond
	dot pos;
}Image;

typedef struct Now{
	short animation;
	short chosing;
}Now;

#endif