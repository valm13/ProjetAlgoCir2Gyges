#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libISEN/GfxLib.h"
#include "libISEN/BmpLib.h"
#include "structure.h"
#include "GestionAffichage.h"
#include "toolbox.h"


/**
 * @brief      { Use to have an aspect of cursor when we write names }
 *
 * @param      Game    pointer on gameParam struct
 * @param      Actual  pointer on Now struct
 */
void TempoTextField(gameParam *Game, Now *Actual)
{
	if(Game->place == 3)
		Actual->animation = (Actual->animation + 1) % 2;
	else
		demandeTemporisation(-1);

}

/**
 * @brief      { Display all data of the gameParam struct for debugging }
 *
 * @param      Game  pointer on gameParam struct
 */
void DisplayGameData(gameParam *Game)
{
	char chaine[20];

	printf("---- Donnees actuelles ----\n\n");
	switch(Game->place){
		case 0:
		strcpy(chaine,"Rules");
		break;
		case 1:
		strcpy(chaine,"Principal");
		break;

		case 2:
		strcpy(chaine,"Versus");
		break;

		case 3:
		strcpy(chaine,"Names");
		break;

		case 4:
		strcpy(chaine,"Location");
		break;

		case 5:
		strcpy(chaine,"En jeu");
		break;
	}
	printf("Place : %s (%d)\n",chaine,Game->place);
	if(Game->place == 0)
		printf("Page : %d\n",Game->page);
	
	printf("Type : %d\n",Game->type);
	
	printf("-- Info Joueurs --\n\n");

	printf("- Joueur 1 -\n");
	if(Game->P1.type == 0)
		printf("Type : Humain\n");
	else 
		printf("Type : I->A\n");
	printf("Name : %s\n",Game->P1.name);
	if(Game->P1.pos == 2)
		printf("Position : South\n");
	else
		printf("Position : North\n");
	printf("Win : %d\n\n",Game->P1.win);

	printf("- Joueur 2-\n");
	if(Game->P2.type == 0)
		printf("Type : Humain\n");
	else 
		printf("Type : I->A\n");
	printf("Name : %s\n",Game->P2.name);
	if(Game->P2.pos == 2)
		printf("Position : South\n");
	else
		printf("Position : North\n");
	printf("Win : %d\n",Game->P2.win);

	printf("GameStatus Action : %d\n",Game->gameStatus.action);




}